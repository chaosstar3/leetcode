// @prob  8. String to Integer (atoi)
// @link  https://leetcode.com/problems/string-to-integer-atoi/
pub struct Solution;
#[allow(dead_code)]
impl Solution {
	pub fn my_atoi(str: String) -> i32 {
		let mut acc: i32 = 0;
		let mut neg: bool = false;
		let mut start: bool = false;

		for c in str.chars(){
			if !start {
				// trim space, detect sign or number
				match c {
					' ' => continue,
					'+' => start = true,
					'-' => {neg = true; start = true}
					'0'..='9' => {acc = c as i32 - 0x30; start = true}
					_ => break,
				}
			} else {
				// parse integer
				match c {
					'0'..='9' => {
						let digit = c as i32 - 0x30;

						if neg {
							if acc < (std::i32::MIN + digit)/10 {
								return std::i32::MIN;
							} else {
								acc = acc*10 - digit;
							}
						} else {
							if acc > (std::i32::MAX - digit)/10 {
								return std::i32::MAX;
							} else {
								acc = acc*10 + digit;
							}
						}
					},
					_ => break,
				}
			}
		}

		return acc;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
		assert_eq!(Solution::my_atoi(String::from("42")), 42);
		assert_eq!(Solution::my_atoi(String::from("   -42")), -42);
		assert_eq!(Solution::my_atoi(String::from("4193 with words")), 4193);
		assert_eq!(Solution::my_atoi(String::from("workds and 987")), 0);
		assert_eq!(Solution::my_atoi(String::from("-91283472332")), std::i32::MIN);
	}

	#[test]
	fn test() {
		assert_eq!(Solution::my_atoi(String::from("+0")), 0);
		assert_eq!(Solution::my_atoi(String::from("+42")), 42);
		assert_eq!(Solution::my_atoi(String::from("-0")), 0);
		assert_eq!(Solution::my_atoi(String::from("123-321")), 123);
		assert_eq!(Solution::my_atoi(String::from("123 321")), 123);
		assert_eq!(Solution::my_atoi(String::from("123x321")), 123);

		assert_eq!(Solution::my_atoi(String::from("2147483647")), std::i32::MAX);
		assert_eq!(Solution::my_atoi(String::from("-2147483648")), std::i32::MIN);
	}
}
