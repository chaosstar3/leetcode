// @prob  9. Palindrome Number
// @link  https://leetcode.com/problems/palindrome-number/
pub struct Solution;
#[allow(dead_code)]
impl Solution {
	pub fn is_palindrome(x: i32) -> bool {
		let mut a = x;
		let mut b = 0;

		while a > 0 {
			b = b*10 + a%10;
			a = a/10;
		}

		return x == b;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
		assert_eq!(Solution::is_palindrome(121), true);
		assert_eq!(Solution::is_palindrome(-121), false);
		assert_eq!(Solution::is_palindrome(10), false);
	}

	#[test]
	fn test() {
		assert_eq!(Solution::is_palindrome(0), true);
		assert_eq!(Solution::is_palindrome(-0), true);
	}
}
