pub struct Solution;
// @prob  3. Longest Substring Without Repeating Characters
// @link  https://leetcode.com/problems/longest-substring-without-repeating-characters/
#[allow(dead_code)]
impl Solution {
	pub fn length_of_longest_substring(s: String) -> i32 {
		let mut occurence: [i32;256] = [-1;256];
		let mut begin = 0;
		let mut max = 0;

		for (i, b) in s.bytes().enumerate() {
			let old = occurence[b as usize];
			occurence[b as usize] = i as i32;

			if old != -1 && old >= begin {
				// repeat
				begin = old + 1;
			}

			let new_max = i as i32 - begin + 1;

			if new_max > max {
				max = new_max;
			}
		}

		return max;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
		assert_eq!(Solution::length_of_longest_substring("abcabcbb".to_string()), 3);
		assert_eq!(Solution::length_of_longest_substring("bbbbb".to_string()), 1);
		assert_eq!(Solution::length_of_longest_substring("pwwkew".to_string()), 3);
	}

	#[test]
	fn test() {
		assert_eq!(Solution::length_of_longest_substring("".to_string()), 0);
	}
}
