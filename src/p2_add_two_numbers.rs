pub struct Solution;
// @prob  2. Add Two Numbers
// @link  https://leetcode.com/problems/add-two-numbers/

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
  pub val: i32,
  pub next: Option<Box<ListNode>>
}

impl ListNode {
  #[inline]
  pub fn new(val: i32) -> Self {
    ListNode {
      next: None,
      val
    }
  }
}

#[allow(dead_code)]
impl Solution {
	pub fn add_two_numbers(l1: Option<Box<ListNode>>, l2: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
		let mut e1 = l1;
		let mut e2 = l2;
		let mut head = ListNode::new(0);
		let mut iter = &mut head.next;
		let mut carry = 0;

		loop {
			let n1 = match e1 {
				None => 0,
				Some(x) => {
					e1 = (*x).next;
					(*x).val
				}
			};

			let n2 = match e2 {
				None => 0,
				Some(x) => {
					e2 = (*x).next;
					(*x).val
				}
			};

			let sum = n1 + n2 + carry;

			if sum == 0 && e1 == None && e2 == None {
				break;
			}

			let digit = sum % 10;
			carry = sum / 10;

			*iter = Some(Box::new(ListNode::new(digit)));
			iter = &mut iter.as_mut().unwrap().next;
		}

		let ret = head.next;

		match ret {
			None => Some(Box::new(ListNode::new(0))),
			_ => ret
		}
	}
}

#[allow(dead_code)]
pub fn conv_num(nums: i32) -> Option<Box<ListNode>> {
	let mut n = nums / 10;
	let mut head = Some(Box::new(ListNode::new(nums % 10)));
	let mut iter = &mut head;

	while n > 0 {
		let digit = n % 10;
		iter.as_mut().unwrap().next = Some(Box::new(ListNode::new(digit)));
		iter = &mut iter.as_mut().unwrap().next;
		n = n / 10;
	}

	head
}

#[allow(dead_code)]
pub fn conv(nums: Vec<i32>) -> Option<Box<ListNode>> {
	let mut head = ListNode::new(0);
	let mut iter = &mut head.next;

	for i in nums {
		*iter = Some(Box::new(ListNode::new(i)));
		iter = &mut iter.as_mut().unwrap().next;
	}

	head.next
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
		assert_eq!(Solution::add_two_numbers(conv(vec![2,4,3]), conv(vec![5,6,4])), conv(vec![7,0,8]));
		assert_eq!(Solution::add_two_numbers(conv(vec![0]), conv(vec![0])), conv(vec![0]));
		assert_eq!(Solution::add_two_numbers(conv(vec![1,6,0,3,3,6,7,2,0,1]), conv(vec![6,3,0,8,9,6,6,9,6,1])), conv(vec![7,9,0,1,3,3,4,2,7,2]));
	}

	#[test]
	fn test() {
		assert_eq!(Solution::add_two_numbers(conv_num(93), conv_num(9)), conv_num(102));
		assert_eq!(Solution::add_two_numbers(conv_num(0), conv_num(42)), conv_num(42));
	}
}
