// @prob  7. Reverse Integer
// @link  https://leetcode.com/problems/reverse-integer/
pub struct Solution;
#[allow(dead_code)]
impl Solution {
	pub fn reverse(x: i32) -> i32 {
		let mut ori: i32 = x;
		let mut rev: i32 = 0;

		while ori != 0 {
			//if (rev > std::i32::MAX / 10) || (rev < std::i32::MIN / 10) {
			//	return 0;
			//}
			//rev = rev*10 + ori%10

			// no overflow in addition with i32::MAX,MIN
			rev = match rev.checked_mul(10) {
				Some(x) => x,
				None => return 0
			};

			rev += ori%10;
			ori /= 10;
		}

		return rev;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
		assert_eq!(Solution::reverse(123), 321);
		assert_eq!(Solution::reverse(-123), -321);
		assert_eq!(Solution::reverse(120), 21);
		assert_eq!(Solution::reverse(1534236469), 0);
	}

	#[test]
	fn test() {
		assert_eq!(Solution::reverse(0), 0);
		assert_eq!(Solution::reverse(1), 1);
		assert_eq!(Solution::reverse(1463847412), 2147483641);
		assert_eq!(Solution::reverse(1563847412), 0);
	}
}
