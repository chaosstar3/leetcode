pub struct Solution;
// @prob  4. Median of Two Sorted Arrays
// @link  https://leetcode.com/problems/median-of-two-sorted-arrays/
#[allow(dead_code)]
impl Solution {
	pub fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
		let mut i1 = nums1.iter();
		let mut i2 = nums2.iter();
		let mut m = Vec::new();

		let mut n1 = i1.next();
		let mut n2 = i2.next();

		loop {
			match (n1, n2) {
				(Some(x), Some(y)) => {
					if x < y {
						m.push(x);
						n1 = i1.next();
					} else {
						m.push(y);
						n2 = i2.next();
					}
				},
				(Some(x), None) => {
					m.push(x);
					n1 = i1.next();
				},
				(None, Some(y)) => {
					m.push(y);
					n2 = i2.next();
				},
				(None, None) => break,
			}
		}

		if m.len() % 2 == 0 {
			return (m[m.len()/2-1] + m[m.len()/2]) as f64 / 2.0;
		} else {
			return *m[m.len()/2] as f64;
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
		assert_eq!(Solution::find_median_sorted_arrays(vec![1,3], vec![2]), 2.0);
		assert_eq!(Solution::find_median_sorted_arrays(vec![1,2], vec![3,4]), 2.5);
	}

	#[test]
	fn test() {
		assert_eq!(Solution::find_median_sorted_arrays(vec![1,3,5,7], vec![2,4,6,8]), 4.5);
		assert_eq!(Solution::find_median_sorted_arrays(vec![1,3,5,7,9], vec![2,4,6,8]), 5.0);
		assert_eq!(Solution::find_median_sorted_arrays(vec![1], vec![1]), 1.0);
		assert_eq!(Solution::find_median_sorted_arrays(vec![-5,-3,-1,1,3,5], vec![-4,-2,0,2,4]), 0.0);
	}
}
