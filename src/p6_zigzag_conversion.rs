// @prob  6. ZigZag Conversion
// @link  https://leetcode.com/problems/zigzag-conversion/
pub struct Solution;
#[allow(dead_code)]
impl Solution {
	pub fn convert(s: String, num_rows: i32) -> String {
		if num_rows <= 1 {
			return s;
		}

		let bytes = s.as_bytes();
		let mut ret = String::new();

		let mut i: usize;
		let mut step;

		for r in 0..num_rows {
			i = r as usize;
			step = 2*(num_rows - r - 1);

			while i < s.len() {
				if step == 0 {
					step = 2*(num_rows - 1) - step;
				}

				ret.push(bytes[i] as char);
				i += step as usize;
				step = 2*(num_rows - 1) - step;
			}
		}

		return ret;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
		assert_eq!(Solution::convert(String::from("PAYPALISHIRING"), 3), "PAHNAPLSIIGYIR");
		assert_eq!(Solution::convert(String::from("PAYPALISHIRING"), 4), "PINALSIGYAHRPI");
		assert_eq!(Solution::convert(String::from("A"), 1), "A");
		assert_eq!(Solution::convert(String::from("AB"), 1), "AB");
	}

	#[test]
	fn test() {
		assert_eq!(Solution::convert(String::from(""), 42), "");
		assert_eq!(Solution::convert(String::from("a"), 1337), "a");
	}
}

