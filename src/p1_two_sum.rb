# @prob  1. Two Sum
# @link  https://leetcode.com/problems/two-sum/
# @param {Integer[]} nums
# @param {Integer} target
# @return {Integer[]}
def two_sum(nums, target)
	h = {}
	nums.each_with_index do |n, i|
			return [h[target-n], i] unless h[target-n].nil?
			h[n] = i
	end
end

require 'test/unit'
class TestSolution < Test::Unit::TestCase
	def test_example
		assert_equal(two_sum([2,7,11,15],9), [0,1])
	end
end

module Solution2
def two_sum(nums, target)
	for i in 0...nums.length
		for j in i+1...nums.length
				return [i,j] if nums[i]+nums[j]==target
		end
	end
end
end