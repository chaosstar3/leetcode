pub struct Solution; // MidSpread
pub struct SolutionDP;
// @prob  5. Longest Palindromic Substring
// @link  https://leetcode.com/problems/longest-palindromic-substring/
#[allow(dead_code)]
impl SolutionDP {
	fn alloc_2darray(size1: usize, size2: usize) -> Box<[Box<[bool]>]> {
		let mut v1 = Vec::with_capacity(size1);
		let mut v2 = Vec::with_capacity(size2);

		for _ in 0..size2 {
			v2.push(false);
		}

		let b2 = v2.into_boxed_slice();

		for _ in 0..size1 {
			v1.push(b2.clone());
		}

		return v1.into_boxed_slice();
	}

	pub fn longest_palindrome(s: String) -> String {
		let mut array = Self::alloc_2darray(s.len(), s.len());
		let barray = s.as_bytes();
		let (mut i, mut j) = (0, 0);
		let (mut index, mut len) = (0, 0);

		for _ in 0..s.len()*(s.len()+1)/2 {
			let mut sub_pldr = false;

			if i == j {
				sub_pldr = true;
			} else if barray[i] == barray[j] {
				if array[i-1][j+1] || (array[i-1][j] && array[i][j+1]) {
					sub_pldr = true;
				}
			}

			if sub_pldr {
				array[i][j] = true;
				index = j;
				len = i - j + 1;
			}

			// diagonal indexing: right-down wise, upper triangle
			if i == s.len() - 1 {
				i = i - j + 1;
				j = 0;
			} else {
				i += 1;
				j += 1;
			}
		}

		return s[index..(index+len)].to_string();
	}
}

#[allow(dead_code)]
impl Solution {
	pub fn longest_palindrome(s: String) -> String {
		let barray = s.as_bytes();

		let mut maxi = 0;
		let mut maxlen;

		if s.len() == 0 {
			maxlen = 0;
		} else {
			maxlen = 1;
		}

		for i in 1..s.len() {
			for off in 0..=1 {
				let mut j = i - 1;
				let mut k = i + off;
				let mut len = off;

				if k >= s.len() {
					break;
				}

				loop {
					if barray[j] == barray[k] {
						len = k - j + 1;
					} else {
						break;
					}

					if j == 0 || k == s.len() - 1 {
						break;
					} else {
						j -= 1;
						k += 1;
					}
				}

				if len > maxlen {
					maxlen = len;
					maxi = i - len/2;
				}
			}
		}

		return s[maxi..(maxi+maxlen)].to_string();
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	type SolutionX = Solution;

	#[test]
	fn example() {
		let mut pldr = SolutionX::longest_palindrome(String::from("babad"));
		assert_eq!(pldr == "bab" || pldr == "aba", true);
		assert_eq!(SolutionX::longest_palindrome(String::from("cbbd")), "bb");

		assert_eq!(SolutionX::longest_palindrome(String::from("a")), "a");
		assert_eq!(SolutionX::longest_palindrome(String::from("")), "");
		assert_eq!(SolutionX::longest_palindrome(String::from("aacdefcaa")), "aa");
		assert_eq!(SolutionX::longest_palindrome(String::from("aaabaaaa")), "aaabaaa");
		pldr = SolutionX::longest_palindrome(String::from("ac"));
		assert_eq!(pldr == "a" || pldr == "c", true);

		// Time limit
		assert_eq!(SolutionX::longest_palindrome(String::from("z".repeat(1000))), "z".repeat(1000));
		let longstr = "cyyoacmjwjubfkzrrbvquqkwhsxvmytmjvbborrtoiyotobzjmohpadfrvmxuagbdczsjuekjrmcwyaovpiogspbslcppxojgbfxhtsxmecgqjfuvahzpgprscjwwutwoiksegfreortttdotgxbfkisyakejihfjnrdngkwjxeituomuhmeiesctywhryqtjimwjadhhymydlsmcpycfdzrjhstxddvoqprrjufvihjcsoseltpyuaywgiocfodtylluuikkqkbrdxgjhrqiselmwnpdzdmpsvbfimnoulayqgdiavdgeiilayrafxlgxxtoqskmtixhbyjikfmsmxwribfzeffccczwdwukubopsoxliagenzwkbiveiajfirzvngverrbcwqmryvckvhpiioccmaqoxgmbwenyeyhzhliusupmrgmrcvwmdnniipvztmtklihobbekkgeopgwipihadswbqhzyxqsdgekazdtnamwzbitwfwezhhqznipalmomanbyezapgpxtjhudlcsfqondoiojkqadacnhcgwkhaxmttfebqelkjfigglxjfqegxpcawhpihrxydprdgavxjygfhgpcylpvsfcizkfbqzdnmxdgsjcekvrhesykldgptbeasktkasyuevtxrcrxmiylrlclocldmiwhuizhuaiophykxskufgjbmcmzpogpmyerzovzhqusxzrjcwgsdpcienkizutedcwrmowwolekockvyukyvmeidhjvbkoortjbemevrsquwnjoaikhbkycvvcscyamffbjyvkqkyeavtlkxyrrnsmqohyyqxzgtjdavgwpsgpjhqzttukynonbnnkuqfxgaatpilrrxhcqhfyyextrvqzktcrtrsbimuokxqtsbfkrgoiznhiysfhzspkpvrhtewthpbafmzgchqpgfsuiddjkhnwchpleibavgmuivfiorpteflholmnxdwewj";
		assert_eq!(SolutionX::longest_palindrome(String::from(longstr)), "xrcrx");
	}

	#[test]
	fn test() {
		assert_eq!(SolutionX::longest_palindrome(String::from("abcdefghijklmnopqrstuvwxyz")).len(), 1);
		assert_eq!(SolutionX::longest_palindrome(String::from("ababababa")), "ababababa");
		assert_eq!(SolutionX::longest_palindrome(String::from("dcbaaabcd")), "dcbaaabcd");
	}
}
