#!/usr/bin/env bash
if [ "$#" -eq 0 ]; then
	echo "Usage: $0 [problem number] [lang]"
	exit 1
fi

case $2 in
	"rust"|"rs"|"")
		ext="rs"
		match_cmd="ag -o 'impl Solution ({([^{}]|(?1))*})'"
		;;
	"ruby"|"rb")
		ext="rb"
		match_cmd="sed -n '/^# @param/,/^end/p'"
		;;
	*)
		echo "Unknown language $2"
		;;
esac

file="src/p$1_*.$ext"

if [ ! -f $file ]; then
	echo "Solution $file not found"
	exit 1
fi

sol=$(eval $match_cmd $file)
echo "$sol"

if (command -v pbcopy > /dev/null); then
	echo "$sol" | pbcopy
elif (command -v clip.exe > /dev/null); then
	echo "$sol" | clip.exe
fi
