require 'pry'
require 'net/http'
require 'net/https'
require 'json'

abort "Usage: #{ARGV[0]} [problem URL] {lang}" if ARGV.empty?
link = ARGV[0]
slug = link.split("/").last
lang, lang_ext = case ARGV[1]
	when "rust", "rs", nil then ["Rust", "rs"]
	when "ruby", "rb" then ["Ruby", "rb"]
	else abort "Unknown language #{ARGV[1]}"
end

# Query
uri = URI('https://leetcode.com/graphql')
query_contents = %w(questionId questionFrontendId title content isPaidOnly
	difficulty likes dislikes similarQuestions topicTags\ { name slug }
	codeSnippets\ { lang langSlug code })
#boundTopicId titleSlug translatedTitle translatedContent isLiked
#contributors\ { username profileUrl avatarUrl __typename }
#stats hints solution\ { id canSeeDetail paidOnly __typename }
#status sampleTestCase metaData judgerAvailable gudgeType mysqlSchemas
#enableRunCode enableTestMode enableDebugger envInfo libraryUrl adminUrl __typename
query = {
	"operationName" => "questionData",
	"variables" => {
		"titleSlug" => slug
	},
	"query" => "query questionData($titleSlug: String!) {\n
		question(titleSlug: $titleSlug) {\n#{query_contents.join("\n")}\n}\n}"
}

# HTTPS request
https = Net::HTTP.new(uri.host, uri.port)
https.use_ssl = true
req = Net::HTTP::Post.new(uri.path)
req['Content-Type'] = 'application/json'
req.body = query.to_json
res = https.request(req)
abort res.to_s unless res.code == "200"
json = JSON.parse(res.body)

GREEN  = "\033[1;32m"
YELLOW = "\033[1;33m"
RED    = "\033[1;31m"
NC     = "\033[0m"
def difficulty_color(lv, str)
	case lv
	when "Easy"   then "#{GREEN}#{str}#{NC}"
	when "Medium" then "#{YELLOW}#{str}#{NC}"
	when "Hard"   then "#{RED}#{str}#{NC}"
	else str
	end
end

q = json["data"]["question"]
id = q["questionId"]
fid = q["questionFrontendId"]
tags = q["topicTags"].map{|tag| tag["name"]}
similar_q = JSON.parse(q["similarQuestions"])
fname = "src/p#{id}_#{slug.gsub('-','_')}.#{lang_ext}"

# Print info
puts "Problem: #{id}. #{difficulty_color(q["difficulty"], q["title"])}"
puts "Tags   : #{tags.join(" ")}"
puts "Similar: #{similar_q.map{|sq| difficulty_color(sq["difficulty"], sq["titleSlug"])}.join(" ")}"
puts "Vote   : #{q["likes"]}/#{q["dislikes"]}"
puts ""
puts q["content"]
puts ""
puts "id #{id} #{fid} different" if id != fid
abort "code exist: #{fname}" if File.exist? fname
puts "generate code: #{fname}"

# TODO: test case generation from example
# Generate template code
code_snippet = q["codeSnippets"].find {|h| h["lang"] == lang}["code"]
template = case lang
when "Rust" then <<RUST_TEMPLATE
// @prob  #{id}. #{q["title"]}
// @link  #{link}
pub struct Solution;
#[allow(dead_code)]
#{code_snippet}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn example() {
	}
}
RUST_TEMPLATE
when "Ruby" then <<RUBY_TEMPLATE
# @prob  #{id}. #{q["title"]}
# @link  #{link}
#{code_snippet}

require 'test/unit'
class TestSolution < Test::Unit::TestCase
	def test_example
	end
end
RUBY_TEMPLATE
end

File.write(fname, template)
